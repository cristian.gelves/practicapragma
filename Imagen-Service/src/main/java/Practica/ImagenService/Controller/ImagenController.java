package Practica.ImagenService.Controller;

import Practica.ImagenService.Entity.Imagen;
import Practica.ImagenService.Repository.ImagenRepository;
import Practica.ImagenService.Services.ImagenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@RestController
@RequestMapping(value = "/imagenes")
public class ImagenController {
    @Autowired
    private ImagenRepository imagenRepository;
    @Autowired
    private ImagenService imagenService;

    @GetMapping
    public ResponseEntity<Imagen> Imagen(@RequestParam(name = "id",required = true)Long id){
        Imagen imagen = imagenService.getImagen(id);
        if (imagen==null){
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(imagen);
    }
    @PostMapping(value = "/Solicitud")
    public ResponseEntity<List<Imagen>> ImagenesId(@RequestBody List<Long> listaId){
        List<Imagen> retorno = imagenService.findAllById(listaId);
            if (retorno==null){
                return ResponseEntity.noContent().build();
            }
        return ResponseEntity.ok(retorno);
    }

    @PostMapping
    public ResponseEntity<Imagen> createImagenString(@RequestBody Imagen imagen )  {

        Imagen imagencreada = Imagen.builder()
                .id(imagen.getId())
                .image(imagen.getImage())
                .build();
        imagenService.createImagen(imagencreada);
        return ResponseEntity.status(HttpStatus.CREATED).body(imagencreada);

    }
    @PostMapping(value = "/Foto")
    public ResponseEntity<Imagen> createImagen(@RequestParam(name = "id") Long id, @RequestPart MultipartFile multipartFile ) throws IOException {

        Imagen imagencreada = Imagen.builder()
                .id(id)
                .image(Base64.getEncoder().encodeToString(multipartFile.getBytes()))
                .build();
        imagenService.createImagen(imagencreada);
        return ResponseEntity.status(HttpStatus.CREATED).body(imagencreada);

    }
    @PutMapping(value = "/{id}")
    public ResponseEntity<Imagen> updateImagen(@PathVariable("id")Long id, @RequestBody Imagen imagen){
        imagen.setId(id);
        Imagen imagenDB = imagenService.updateImagen(imagen);
        if (imagenDB==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(imagenDB);
    }
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Imagen> deleteImagen(@PathVariable("id") Long id) {
        Imagen imagenDelete = imagenService.deleteImagen(id);
        if (imagenDelete == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(imagenDelete);
    }



}
