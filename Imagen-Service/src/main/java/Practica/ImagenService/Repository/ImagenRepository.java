package Practica.ImagenService.Repository;

import Practica.ImagenService.Entity.Imagen;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImagenRepository extends MongoRepository<Imagen, Long> {

}