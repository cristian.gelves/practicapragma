package Practica.ImagenService;

import Practica.ImagenService.Repository.ImagenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class ImagenServiceApplication implements CommandLineRunner {

    @Autowired
    private ImagenRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(ImagenServiceApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {


    }

}