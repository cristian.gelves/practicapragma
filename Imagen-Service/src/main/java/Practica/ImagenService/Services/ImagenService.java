package Practica.ImagenService.Services;

import Practica.ImagenService.Entity.Imagen;

import java.util.List;

public interface ImagenService {
    public Imagen createImagen(Imagen imagen);

    public Imagen getImagen(Long ID);

    public Imagen updateImagen(Imagen imagen);

    public Imagen deleteImagen(Long ID);

    public Imagen showImages(List<Long> id);

    public List<Imagen> findAllById(List<Long> ID);
}
