package Practica.ImagenService.Services;

import Practica.ImagenService.Entity.Imagen;
import Practica.ImagenService.Repository.ImagenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ImagenServiceImpl implements ImagenService {
    @Autowired
    private ImagenRepository imagenRepository;


    @Override
    public Imagen createImagen(Imagen imagen) {
        Imagen imagenDB = imagenRepository.findById(imagen.getId()).orElse(null);
        if (imagenDB != null) {
            return imagenDB;
        }
        imagen.setStatus("CREATED");
        return  imagenRepository.save(imagen);
    }

    @Override
    public Imagen getImagen(Long ID) {

        return imagenRepository.findById(ID).orElse(null);
    }

    @Override
    public Imagen updateImagen(Imagen imagen) {
        Imagen imagenDB = imagenRepository.findById(imagen.getId()).orElse(null);
        if (imagenDB != null) {
            return imagenDB;
        }
        imagenDB.setImage(imagen.getImage());
        imagenDB.setId(imagen.getId());
        return imagenRepository.save(imagenDB);
    }

    @Override
    public Imagen deleteImagen(Long ID) {
        Imagen imagenDB = getImagen(ID);
        if (null == imagenDB) {
            return null;
        }
        imagenDB.setStatus("DELETED");
        return imagenRepository.save(imagenDB);
    }

    @Override
    public Imagen showImages(List<Long> id) {
        return null;
    }

    @Override
    public List<Imagen> findAllById(List<Long> ID) {
        List<Imagen> retorno = new ArrayList<>();
        for (Long i:ID){
            Imagen imagen = getImagen(i);
            retorno.add(imagen);
        }
        return retorno;
    }
}

