package Practica.Backend.Practica.Service;

import Practica.Backend.Practica.DTO.ClientWithImage;
import Practica.Backend.Practica.Entity.Clients;
import Practica.Backend.Practica.Model.Imagen;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface ClientsService {
    public List<ClientWithImage> listAllClients();

    public ClientWithImage getClients(Long numeroIdentificacion);

    public ClientWithImage showClients(Long numeroIdentificacion, String TipoDeIdentificacion);

    public ClientWithImage crear(Clients cliente, Imagen imagen );

    public Clients crearimagen(ClientWithImage clientWithImage);


    public ClientWithImage updateClientsimage(Clients clients, MultipartFile multipartFile) throws IOException;

    public ClientWithImage deleteClients(Long id);

    public List<ClientWithImage> findAllByEdad(Long edad);

     public  Clients createclientfoto(Clients clients, MultipartFile multipartFile) throws IOException;



}
