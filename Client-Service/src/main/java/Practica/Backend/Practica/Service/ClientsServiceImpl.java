package Practica.Backend.Practica.Service;

import Practica.Backend.Practica.DTO.ClientWithImage;
import Practica.Backend.Practica.Entity.Clients;
import Practica.Backend.Practica.FeignClients.ImagenClient;
import Practica.Backend.Practica.Model.Imagen;
import Practica.Backend.Practica.Repository.ClientsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor

public class ClientsServiceImpl implements ClientsService {
    @Autowired
    private ClientsRepository clientsRepository;
    @Autowired
    private ImagenClient imagenClient;


    @Override
    public List<ClientWithImage> listAllClients() {
        List<Long> clientes = clientsRepository.findAll()
                .stream()
                .map(Clients::getNumeroIdentificacion)
                .collect(Collectors.toList());
        ResponseEntity<List<Imagen>> imagenes = imagenClient.ImagenesId(clientes);
        List<Clients> clientes2 = clientsRepository.findAll();
        List<ClientWithImage> clientWithImages = new ArrayList<>();
        for (Clients i:clientes2){
            for (Imagen j:imagenes.getBody()){
                if (i.getNumeroIdentificacion().equals(j.getId())){
                    ClientWithImage clientWithImagen = crear(i,j);

                    clientWithImages.add(clientWithImagen);
                }
            }
        }
        return clientWithImages;
    }

    @Override
    public ClientWithImage getClients(Long numeroIdentificacion) {
        Imagen imagen = imagenClient.Imagen(numeroIdentificacion).getBody();
        Clients cliente = clientsRepository.findById(numeroIdentificacion).orElse(null);
        return crear(cliente,imagen);

    }

    @Override
    public ClientWithImage showClients(Long numeroIdentificacion, String TipoDeIdentificacion) {
        Imagen imagen = imagenClient.Imagen(numeroIdentificacion).getBody();
        Clients cliente = clientsRepository.findByNumeroIdentificacionAndTipoIdentificacion(numeroIdentificacion, TipoDeIdentificacion);
        return  crear(cliente,imagen);
    }




    @Override
    public ClientWithImage updateClientsimage(Clients clients, MultipartFile multipartFile) throws IOException {
        ClientWithImage clientDB = getClients(clients.getNumeroIdentificacion());
        if (null == clientDB) {
            return null;
        }
        clientDB.setNombres(clients.getNombres());
        clientDB.setApellidos(clients.getApellidos());
        clientDB.setEdad(clients.getEdad());
        clientDB.setCiudadNacimiento(clients.getCiudadNacimiento());
        clientDB.setTipoIdentificacion(clients.getTipoIdentificacion());
        imagenClient.updateImagen(clientDB.getNumeroIdentificacion(), Imagen.builder()
                        .id(clientDB.getNumeroIdentificacion())
                .image(Base64.getEncoder().encodeToString(multipartFile.getBytes())).build());
        clientsRepository.save(crearimagen(clientDB));
        return (clientDB);

    }

    @Override
    public ClientWithImage deleteClients(Long id) {
        ClientWithImage clientDB = getClients(id);
        if (null == clientDB) {
            return null;
        }
        clientDB.setStatus("DELETED");
        imagenClient.deleteImagen(id);
        clientsRepository.save(crearimagen(clientDB));
        return (clientDB);
    }

    @Override
    public List<ClientWithImage> findAllByEdad(Long edad) {
        List<Long> clientes = clientsRepository.findByEdadGreaterThanEqual(edad)
                .stream()
                .map(Clients::getNumeroIdentificacion)
                .collect(Collectors.toList());
        ResponseEntity<List<Imagen>> imagenes = imagenClient.ImagenesId(clientes);
        List<Clients> clientes2 = clientsRepository.findByEdadGreaterThanEqual(edad);
        List<ClientWithImage> clientWithImages = new ArrayList<>();
        for (Clients i:clientes2){
            for (Imagen j:imagenes.getBody()){
                if (i.getNumeroIdentificacion()==j.getId()){

                    clientWithImages.add(crear(i,j));
                }
            }
        }
        return clientWithImages;
    }

    @Override
    public Clients createclientfoto(Clients clients, MultipartFile multipartFile) throws IOException {
        Clients clientsDB = clientsRepository.findByNumeroIdentificacionAndTipoIdentificacion(clients.getNumeroIdentificacion(),clients.getTipoIdentificacion());
        if (clientsDB!= null){
            return clientsDB;
        }

        clients.setStatus("CREATED");
        clientsRepository.save(clients);
        imagenClient.createImagenString(Imagen.builder()
                        .id(clients.getNumeroIdentificacion())
                        .image(Base64.getEncoder().encodeToString(multipartFile.getBytes()))
                         .build()) ;


        return clients;
    }
    @Override
    public Clients crearimagen(ClientWithImage clientWithImage){
        return Clients.builder()
                .apellidos(clientWithImage.getApellidos())
                .ciudadNacimiento(clientWithImage.getCiudadNacimiento())
                .edad(clientWithImage.getEdad())
                .nombres(clientWithImage.getNombres())
                .status(clientWithImage.getStatus())
                .tipoIdentificacion(clientWithImage.getTipoIdentificacion())
                .numeroIdentificacion(clientWithImage.getNumeroIdentificacion())
                .build();
    }

    @Override
    public ClientWithImage crear(Clients cliente, Imagen imagen ){
        ClientWithImage imagenconcliente = ClientWithImage.builder()
                .numeroIdentificacion(cliente.getNumeroIdentificacion())
                .Imagen(imagen.getImage())
                .tipoIdentificacion(cliente.getTipoIdentificacion())
                .status(cliente.getStatus())
                .nombres(cliente.getNombres())
                .edad(cliente.getEdad())
                .ciudadNacimiento(cliente.getCiudadNacimiento())
                .apellidos(cliente.getApellidos())
                .build();
        return imagenconcliente;
    }



}
