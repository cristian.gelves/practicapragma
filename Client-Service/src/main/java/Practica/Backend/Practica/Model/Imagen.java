package Practica.Backend.Practica.Model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Imagen implements Serializable {
    private Long id;

    private String image;

    private String Status;

}
