package Practica.Backend.Practica.RestController;

import Practica.Backend.Practica.DTO.ClientWithImage;
import Practica.Backend.Practica.Entity.Clients;
import Practica.Backend.Practica.Repository.ClientsRepository;
import Practica.Backend.Practica.Service.ClientsService;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/clients")
public class ClientsController {
    @Autowired
    private ClientsService clientsService;
    @Autowired
    private ClientsRepository clientsRepository;

    @GetMapping
    public ResponseEntity<List<ClientWithImage>> listClients(@RequestParam(name = "edad", required = false) Long edad) {

        List<ClientWithImage> clients = new ArrayList<>();
        if (null == edad) {
            clients = clientsService.listAllClients();
            if (clients.isEmpty()) {
                return ResponseEntity.noContent().build();
            }
        } else {
            clients = clientsService.findAllByEdad(edad);
            if (clients.isEmpty()) {
                return ResponseEntity.notFound().build();
            }
        }

        return ResponseEntity.ok(clients);
    }

    @GetMapping(value = "/{numeroIdentificacion}/{tipoIdentificacion}")
    public ResponseEntity<ClientWithImage> getclient(@PathVariable("numeroIdentificacion") Long numeroIdentificacion, @PathVariable("tipoIdentificacion") String tipoIdentificacion) {
        ClientWithImage clients = clientsService.showClients(numeroIdentificacion, tipoIdentificacion);
        if (null == clients) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(clients);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<Clients> createClientFoto(@Valid @RequestPart Clients clients, BindingResult result,@RequestPart MultipartFile multipartFile) throws IOException {
        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,this.formatMessage(result));
        }
        Clients clientsCreated = clientsService.createclientfoto(clients, multipartFile);
        return ResponseEntity.status(HttpStatus.CREATED).body(clientsCreated);
    }


    @PutMapping(value = "/{id}/imagen")
    public ResponseEntity<ClientWithImage> updateClient(@PathVariable("id") Long id, @RequestPart Clients clients,@RequestPart MultipartFile multipartFile) throws IOException {
        clients.setNumeroIdentificacion(id);
        ClientWithImage clientsDB = clientsService.updateClientsimage(clients,multipartFile);
        if (clientsDB == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(clientsDB);

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<ClientWithImage> deleteClient(@PathVariable("id") Long id) {
        ClientWithImage clientsDelete = clientsService.deleteClients(id);
        if (clientsDelete == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(clientsDelete);
    }
    private String formatMessage( BindingResult result){
        List<Map<String,String>> errors = result.getFieldErrors().stream()
                .map(err ->{
                    Map<String,String>  error =  new HashMap<>();
                    error.put(err.getField(), err.getDefaultMessage());
                    return error;

                }).collect(Collectors.toList());
        ErroMessage errorMessage = ErroMessage.builder()
                .code("01")
                .messages(errors).build();
        ObjectMapper mapper = new ObjectMapper();
        String jsonString="";
        try {
            jsonString = mapper.writeValueAsString(errorMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }


}
