package Practica.Backend.Practica.Repository;

import Practica.Backend.Practica.Entity.Clients;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ClientsRepository extends JpaRepository<Clients, Long> {
    public Clients findByNumeroIdentificacionAndTipoIdentificacion(Long numeroIdentificacion, String TipoIdentificacion);

    public List<Clients> findByEdadGreaterThanEqual(Long Edad);


}
