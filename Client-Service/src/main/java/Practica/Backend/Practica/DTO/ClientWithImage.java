package Practica.Backend.Practica.DTO;

import Practica.Backend.Practica.Entity.Clients;
import Practica.Backend.Practica.Model.Imagen;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ClientWithImage implements Serializable {

    private Long numeroIdentificacion;

    private String tipoIdentificacion;

    private String status;

    private String nombres;

    private String apellidos;

    private Long edad;

    private String ciudadNacimiento;

    private String Imagen;


}
