package Practica.Backend.Practica.Entity;

import Practica.Backend.Practica.DTO.ClientWithImage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.io.Serializable;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Clients implements Serializable {

    @Id
    private Long numeroIdentificacion;
    @NotEmpty(message = "Debe ingresar tipo de documento")
    private String tipoIdentificacion;


    private String status;
    @NotEmpty(message = "Debe ingresar un nombre")
    private String nombres;
    @NotEmpty(message = "Debe ingresar un apellido")
    private String apellidos;
    @Positive(message = "La edad debe ser mayor a cero")
    private Long edad;



    private String ciudadNacimiento;






}
