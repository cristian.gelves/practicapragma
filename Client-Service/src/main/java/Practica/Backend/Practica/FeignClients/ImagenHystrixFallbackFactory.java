package Practica.Backend.Practica.FeignClients;

import Practica.Backend.Practica.Model.Imagen;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class ImagenHystrixFallbackFactory implements ImagenClient{
    @Override
    public ResponseEntity<Imagen> Imagen(Long id) {
        Imagen imagen = Imagen.builder()
                .image("none")
                .Status("none")
                .build();
        return ResponseEntity.ok(imagen);
    }

    @Override
    public ResponseEntity<Imagen> createImagen(Long id, MultipartFile multipartFile) throws IOException {
        Imagen imagen = Imagen.builder()
                .image("none")
                .Status("none")
                .build();
        return ResponseEntity.ok(imagen);
    }

    @Override
    public ResponseEntity<Imagen> createImagenString(Imagen imagen) {
        Imagen imagen1 = Imagen.builder()
                .image("none")
                .Status("none")
                .build();
        return ResponseEntity.ok(imagen1);
    }

    @Override
    public ResponseEntity<Imagen> updateImagen(Long id, Imagen imagen) {
        Imagen imagen2 = Imagen.builder()
                .image("none")
                .Status("none")
                .build();
        return ResponseEntity.ok(imagen2);
    }

    @Override
    public ResponseEntity<Imagen> deleteImagen(Long id) {
        Imagen imagen = Imagen.builder()
                .image("none")
                .Status("none")
                .build();
        return ResponseEntity.ok(imagen);
    }

    @Override
    public ResponseEntity<List<Imagen>> ImagenesId(List<Long> listaId) {
        List<Imagen> imagens = new ArrayList<>();
        Imagen imagen = Imagen.builder()
                .image("none")
                .Status("none")
                .build();
        imagens.add(imagen);
        return ResponseEntity.ok(imagens);
    }
}
