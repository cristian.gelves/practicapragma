package Practica.Backend.Practica.FeignClients;

import Practica.Backend.Practica.Model.Imagen;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@FeignClient(name = "Imagen-Service", fallback = ImagenHystrixFallbackFactory.class)
public interface ImagenClient {
    @GetMapping(value = "/imagenes")
    public ResponseEntity<Imagen> Imagen(@RequestParam(name = "id",required = true)Long id);
    @PostMapping(value = "/imagenes/foto")
    public ResponseEntity<Imagen> createImagen(@RequestParam(name = "id") Long id, @RequestPart MultipartFile multipartFile ) throws IOException;
    @PostMapping(value = "/imagenes")
    public ResponseEntity<Imagen> createImagenString(@RequestBody Imagen imagen);
    @PutMapping(value = "/imagenes/{id}")
    public ResponseEntity<Imagen> updateImagen(@PathVariable("id")Long id, @RequestBody Imagen imagen);
    @DeleteMapping(value = "/imagenes/{id}")
    public ResponseEntity<Imagen> deleteImagen(@PathVariable("id") Long id);
    @PostMapping(value = "/imagenes/Solicitud")
    public ResponseEntity<List<Imagen>> ImagenesId(@RequestBody List<Long> listaId);


}
