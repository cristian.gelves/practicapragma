package Practica.Backend.Practica.Service;

import Practica.Backend.Practica.DTO.ClientWithImage;
import Practica.Backend.Practica.Entity.Clients;
import Practica.Backend.Practica.Model.Imagen;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

public class Datos {
    public final static List<Clients> CLIENTS_LIST = Arrays.asList(new Clients(1L,"CC","CREATED","JUAN","PEPE",1l,"CUCUTA"),
            new Clients(2L,"CC","CREATED","YEISON","MARTINEZ",10l,"TUNJA"),
            new Clients(3L,"CC","CREATED","JHONNY","EDUARDO",20l,"PARIS"));

    public final static List<Imagen> IMAGEN_LIST = Arrays.asList(new Imagen(1L,"imagen1","CREATED"),
            new Imagen(2L,"imagen2","CREATED"),
            new Imagen(3L,"imagen3","CREATED"));

    public final static ResponseEntity<List<Imagen>> LIST_RESPONSE_ENTITY_IMAGEN = new ResponseEntity(IMAGEN_LIST, HttpStatus.OK);

    public final static List<Long> LONG_LIST = Arrays.asList(1L,2L,3L);

    public final static Imagen IMAGEN = new Imagen(1L,"imagen1","CREATED");

    public final static Imagen IMAGEN_DELETED = new Imagen(1L,"imagen1","DELETED");

    public final static Long LONG = 1L;

    public final static Clients CLIENTS = new Clients(1L,"CC","CREATED","JUAN","PEPE",1l,"CUCUTA") ;

    public final static ResponseEntity<Imagen> IMAGEN_RESPONSE_ENTITY = new ResponseEntity<>(IMAGEN,HttpStatus.OK);

    public final static ResponseEntity<Imagen> IMAGEN_RESPONSE_ENTITY_DELETED = new ResponseEntity<>(IMAGEN_DELETED,HttpStatus.OK);

    public final static ClientWithImage CLIENT_WITH_IMAGE = new ClientWithImage(1L,"CC","CREATED","JUAN","PEPE",1l,"CUCUTA","imagen1");

    public final static ClientWithImage CLIENT_WITH_IMAGE_DELETED = new ClientWithImage(1L,"CC","DELETED","JUAN","PEPE",1l,"CUCUTA","imagen1");

    public final static Clients CLIENTS_DELETED = new Clients(1L,"CC","DELETED","JUAN","PEPE",1l,"CUCUTA") ;
}
