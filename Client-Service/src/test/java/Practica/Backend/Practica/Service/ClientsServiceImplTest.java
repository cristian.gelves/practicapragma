package Practica.Backend.Practica.Service;

import Practica.Backend.Practica.DTO.ClientWithImage;
import Practica.Backend.Practica.Entity.Clients;
import Practica.Backend.Practica.FeignClients.ImagenClient;
import Practica.Backend.Practica.Model.Imagen;
import Practica.Backend.Practica.Repository.ClientsRepository;
import org.checkerframework.checker.nullness.Opt;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.meta.When;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ClientsServiceImplTest {

    private static final Long NUMERODEIDENTIFICACION = 1L;
    private static final String TIPODEIDENTIFICACION = "CC";
    private static final String STATUS = "CREATED";
    private static final String NOMBRES = "JUAN";
    private static final String APELLIDOS = "PEPE";
    private static final Long EDAD = 1L;
    private static final String CIUDADDENACIMIENTO = "CUCUTA";
    private static final String IMAGEN = "imagen1";
    private static final byte[] ARCHIVO = new byte[0];

    @InjectMocks
    ClientsServiceImpl clientsService;

    @Mock
    ClientsRepository clientsRepository;

    @Mock
    ClientWithImage clientWithImage;

    @Mock
    ImagenClient imagenClient;

    @Mock
    Clients clients;


    @Mock
    ResponseEntity responseEntity;

    @Mock
    Imagen imagen;

    @Mock
    MultipartFile multipartFile;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void listAllClients() {
        when(clientsRepository.findAll()).thenReturn(Datos.CLIENTS_LIST);

        when(imagenClient.ImagenesId(anyList())).thenReturn(Datos.LIST_RESPONSE_ENTITY_IMAGEN);
        List<ClientWithImage> clientWithImageList = clientsService.listAllClients();
        assertFalse(clientWithImageList.isEmpty());


    }

    @Test
    void getClients() {
        when(imagenClient.Imagen(anyLong())).thenReturn(Datos.IMAGEN_RESPONSE_ENTITY);
        when(clientsRepository.findById(anyLong())).thenReturn(Optional.of(Datos.CLIENTS));
        Long numeroIdentificacion = 1L;
        Optional<Imagen> imagen = Optional.ofNullable(imagenClient.Imagen(numeroIdentificacion).getBody());
        Optional<Clients> cliente = clientsRepository.findById(numeroIdentificacion);
        assertTrue(cliente.isPresent());
        assertEquals("JUAN",cliente.get().getNombres());
        assertTrue(imagen.isPresent());
        assertEquals("imagen1",imagen.get().getImage());


    }

    @Test
    void showClients() {
        when(clientsRepository.findByNumeroIdentificacionAndTipoIdentificacion(anyLong(),any())).thenReturn(Datos.CLIENTS);
        when(imagenClient.Imagen(anyLong())).thenReturn(Datos.IMAGEN_RESPONSE_ENTITY);
        Optional<ClientWithImage> clientWithImage = Optional.ofNullable(clientsService.showClients(anyLong(), "CC"));
        assertTrue(clientWithImage.isPresent());



    }

    @Test
    void updateClientsimage() throws IOException {
        when(imagenClient.Imagen(anyLong())).thenReturn(Datos.IMAGEN_RESPONSE_ENTITY);
        when(clientsRepository.findById(anyLong())).thenReturn(Optional.of(Datos.CLIENTS));

        when(imagenClient.Imagen(anyLong())).thenReturn(Datos.IMAGEN_RESPONSE_ENTITY);

        when(imagenClient.updateImagen(anyLong(),any())).thenReturn(Datos.IMAGEN_RESPONSE_ENTITY);
        when(clientsRepository.save(any())).thenReturn(Datos.CLIENTS);

        when(multipartFile.getBytes()).thenReturn(ARCHIVO);

        Optional<ClientWithImage> clientWithImage = Optional.ofNullable(clientsService.updateClientsimage(clients, multipartFile));
        assertTrue(clientWithImage.isPresent());


    }

    @Test
    void deleteClients() {
        when(imagenClient.Imagen(anyLong())).thenReturn(Datos.IMAGEN_RESPONSE_ENTITY);
        when(clientsRepository.findById(anyLong())).thenReturn(Optional.of(Datos.CLIENTS));
        when(imagenClient.deleteImagen(anyLong())).thenReturn(Datos.IMAGEN_RESPONSE_ENTITY_DELETED);
        when(clientsRepository.save(any())).thenReturn(Datos.CLIENTS_DELETED);
        Optional<ClientWithImage> clientWithImage = Optional.ofNullable(clientsService.deleteClients(NUMERODEIDENTIFICACION));
        assertTrue(clientWithImage.isPresent());
    }

    @Test
    void findAllByEdad() {
        when(clientsRepository.findByEdadGreaterThanEqual(anyLong())).thenReturn(Datos.CLIENTS_LIST);
        when(imagenClient.ImagenesId(anyList())).thenReturn(Datos.LIST_RESPONSE_ENTITY_IMAGEN);
        Optional<List<ClientWithImage>> clientWithImageList = Optional.ofNullable(clientsService.findAllByEdad(EDAD));
        assertTrue(clientWithImageList.isPresent());
    }

    @Test
    void createclientfoto() throws IOException {
        when(clientsRepository.findByNumeroIdentificacionAndTipoIdentificacion(anyLong(),any())).thenReturn(Datos.CLIENTS);
        Optional<Clients> clientes = Optional.ofNullable(clientsService.createclientfoto(clients, multipartFile));
        assertTrue(clientes.isPresent());
        assertEquals(1L,clientes.get().getNumeroIdentificacion());
    }


}