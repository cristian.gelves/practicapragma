package Practica.Backend.Practica;

import Practica.Backend.Practica.Entity.Clients;
import Practica.Backend.Practica.Repository.ClientsRepository;
import Practica.Backend.Practica.Service.ClientsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PracticaPragmaApplicationTests {

    @Autowired
    private ClientsRepository repo;
    @Autowired
    private ClientsService clientsService;

    @Test
    void contextLoads() {
        Clients cliente = Clients.builder()
                .apellidos("cristian")
                .ciudadNacimiento("Bucaramanga")
                .edad(20L)
                .numeroIdentificacion(15L)
                .nombres("gelves")
                .status("CREATED")
                .tipoIdentificacion("CC")
                .build();
        repo.save(cliente);
        Clients client = new Clients();
        client.setTipoIdentificacion("cc");
        client.setNumeroIdentificacion(22L);
        client.setEdad(21L);
        client.setCiudadNacimiento("bucaramanga");
        client.setApellidos("Gelves Higuera");
        client.setNombres("Cristian Camilo");

        repo.save(client);
        repo.findAll();
        repo.findByEdadGreaterThanEqual(20L);
        repo.findByNumeroIdentificacionAndTipoIdentificacion(1007868424L, "cc");
    }

}
